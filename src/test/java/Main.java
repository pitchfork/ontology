import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        OntModel model = ModelFactory.createOntologyModel();
        model.read(new FileInputStream("fitness-app.owl"), "");

        Selector selector = new Selector(model, "http://www.semanticweb.org/egor/ontologies/2016/11/fitness-app#");

        System.out.println("\nДобро пожаловать в фитнесс-приложение для выбора диеты и программы тренировок\n");
        System.out.println("В данный момент для выбора доступно " + selector.getNumberOfDiets() + " диеты и " + selector.getNumberOfTrainingPrograms() + " программ тренировок\n");

        // Фильтры
        selector.filterDietsByClass("BulkingDiet");
        selector.filterDietsByProperty("dietTermInMonths", (stmt) -> { return stmt.getInt() > 2; });
        selector.filterDietsByProperty("developedBy", (stmt) -> { return ((Resource)stmt.getObject()).getProperty(selector.getProperty("authorLastName")).getString().equals("Wendler"); });

        System.out.println("Запрос 1:");
        System.out.println("- Диета для набора мышечной массы");
        System.out.println("- Срок диеты - больше двух месяцев");
        System.out.println("- Была разработана Джимом Вендлером");
        selector.printSelectedDiets();
        selector.resetDiets();
        System.out.println();

        selector.filterTrainingProgramsByClass("LosingWeightProgram");
        selector.filterTrainingProgramsByProperty("trainingProgramNumberOfTrainingDays", (stmt) -> { return stmt.getInt() == 4; });
        selector.filterTrainingProgramsByProperty("trainingProgramTermInMonths", (stmt) -> { return stmt.getInt() == 2; });

        System.out.println("Запрос 2:");
        System.out.println("- Программа тренировок для похудения");
        System.out.println("- Количество тренировочных дней в неделю - 4");
        System.out.println("- Срок программы тренировок - 2 месяца");
        selector.printSelectedTrainingPrograms();
        selector.resetTrainingPrograms();
    }

}
