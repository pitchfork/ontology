import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LB3 {

    public static void main(String[] args) throws FileNotFoundException {
        OntModel model = ModelFactory.createOntologyModel();
        model.read(new FileInputStream("fitness-app.owl"), "");

        Selector selector = new Selector(model, "http://www.semanticweb.org/egor/ontologies/2016/11/fitness-app#");

        System.out.println("Количество классов онтологии: " + selector.getNumberOfClasses());
        System.out.println("Количество экземпляров онтологии: " + selector.getNumberOfIndividuals());
        System.out.println("Классы со свойством dietName:");
        selector.getListOfClassesWithProperty("dietName").forEach(System.out::println);
        System.out.println();

        selector.filterDietsByClass("BulkingDiet");
        System.out.println("Запрос 1:");
        System.out.println("- Диета для набора мышечной массы");
        selector.printSelectedDiets();
        selector.resetDiets();
    }

}
