import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.*;

public class Selector {
    private final OntModel ont;
    private String baseURL;
    private List<Individual> trainingPrograms;
    private List<Individual> diets;
    private List<Individual> selectedDiets;
    private List<Individual> selectedTrainingPrograms;

    public Selector(OntModel ont, String baseURL){
        this.ont = ont;
        this.baseURL = baseURL;
        this.diets = ont.listIndividuals().toList().stream().filter(e -> e.hasProperty(getProperty("dietName"))).collect(Collectors.toList());
        this.trainingPrograms = ont.listIndividuals().toList().stream().filter(e -> e.hasProperty(getProperty("trainingProgramName"))).collect(Collectors.toList());
        resetDiets();
        resetTrainingPrograms();
    }

    public int getNumberOfDiets() {
        return diets.size();
    }

    public int getNumberOfTrainingPrograms() {
        return trainingPrograms.size();
    }

    public Property getProperty(String prop){
        return ont.getProperty(baseURL + prop);
    }

    public OntClass getClass(String clazz){
        return ont.getOntClass(baseURL + clazz);
    }

    public void resetDiets(){
        selectedDiets = new LinkedList<>(diets);
    }

    public void resetTrainingPrograms(){
        selectedTrainingPrograms = new LinkedList<>(trainingPrograms);
    }

    public void filterDietsByClass(String clazz){
        selectedDiets = filterByClass(selectedDiets, clazz);
    }

    public void filterTrainingProgramsByClass(String clazz){
        selectedTrainingPrograms = filterByClass(selectedTrainingPrograms, clazz);
    }

    public List<Individual> filterByClass(List<Individual> list, String clazz){
        return list.stream().filter(e -> e.getOntClass().getURI().equals(baseURL + clazz)).collect(Collectors.toList());
    }

    public void filterDietsByProperty(String prop, Function<Statement, Boolean> filter){
        selectedDiets = filterByProperty(selectedDiets, prop, filter);
    }

    public void filterTrainingProgramsByProperty(String prop, Function<Statement, Boolean> filter){
        selectedTrainingPrograms = filterByProperty(selectedTrainingPrograms, prop, filter);
    }

    public List<Individual> filterByProperty(List<Individual> list, String prop, Function<Statement, Boolean> filter){
        return list.stream().filter(e -> filter.apply(e.getProperty(getProperty(prop)))).collect(Collectors.toList());
    }

    public void printSelectedDiets(){
        System.out.println("Выбрано " + " диет: " + selectedDiets.size());
        for (Individual diet: selectedDiets)
            System.out.println(diet.getProperty(getProperty("dietName")).getString());
    }

    public void printSelectedTrainingPrograms(){
        System.out.println("Выбрано " + " тренировочных программ: " + selectedTrainingPrograms.size());
        for (Individual trainingProgram: selectedTrainingPrograms)
            System.out.println(trainingProgram.getProperty(getProperty("trainingProgramName")).getString());
    }

    public int getNumberOfClasses (){
        return ont.listNamedClasses().toList().size();
    }

    public int getNumberOfIndividuals(){
        return ont.listIndividuals().toList().size();
    }

    public List<OntClass> getListOfClassesWithProperty(String prop){
        List<OntClass> resultList = new LinkedList<>();
        for(Individual i: ont.listIndividuals().toList()){
            if(i.hasProperty(getProperty(prop)) && !resultList.contains(i.getOntClass()))
                resultList.add(i.getOntClass());
        }
        return resultList;
    }

}
